
git "/var/www/sites/" do
  repository "git@bitbucket.org:corey_white/chef_irelo_lamp.git"
  revision "master"
  action :sync
  user "root"
end
